using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

[RequireComponent (typeof (MeshRenderer))]
[RequireComponent (typeof (MeshFilter))]
[RequireComponent (typeof (MeshCollider))]
[ExecuteInEditMode]
public class AreaCollider : MonoBehaviour
{
	[SerializeField]
	public List<Vector3> coords = new List<Vector3>();
	public bool showHandles; 
	
	private Mesh mesh;
	public int NbCoords { get { return coords.Count; } }
	private string AreaName { get { return transform.name + "Area"; } }

	public virtual void CopyValuesFrom(AreaCollider other)
	{
		coords = other.coords;
		showHandles = other.showHandles;
		mesh = other.mesh;
	}

	public void OnEnable()
	{
		MeshCollider meshCollider = gameObject.GetComponent<MeshCollider>();
// ReSharper disable once ConvertIfStatementToNullCoalescingExpression
		if (meshCollider == null)
			meshCollider = gameObject.AddComponent<MeshCollider>();
		MeshFilter meshFilter = gameObject.GetComponent<MeshFilter>();
// ReSharper disable once ConvertIfStatementToNullCoalescingExpression
		if (meshFilter == null)
			meshFilter = gameObject.AddComponent<MeshFilter>();
		MeshRenderer meshRenderer = gameObject.GetComponent<MeshRenderer>();
// ReSharper disable once ConvertIfStatementToNullCoalescingExpression
		if(meshRenderer == null)
			meshRenderer = gameObject.AddComponent<MeshRenderer>();
		if(meshRenderer.sharedMaterial == null)
			meshRenderer.sharedMaterial = new Material(Shader.Find("Hidden/AreaTrigger"));
		
		mesh = meshFilter.sharedMesh;
		if(mesh == null)
		{
			mesh = new Mesh {name = AreaName};

			CreateArea(ref mesh, coords);
			meshCollider.sharedMesh = mesh;
			meshFilter.sharedMesh = mesh;
		}

		meshCollider.isTrigger = false;
		meshCollider.convex = false;
	}

	public void UpdateHandles()
	{
		#if UNITY_EDITOR
		Undo.RecordObject(this,"Adjust Area Trigger");
		
		bool updateArea = false;
		Handles.color = Color.grey;
		for(int i = 0; i < NbCoords; ++i)
		{
			Vector3 t = transform.InverseTransformPoint(Handles.PositionHandle(transform.TransformPoint(coords[i]), Quaternion.identity));
			if(coords[i] != t)
			{
				coords[i] = t;
				updateArea = true;
			}
		}	
		
		if(updateArea)
			CreateArea(ref mesh, coords);
		
		if(NbCoords > 1)
		{	
			Handles.Label(transform.TransformPoint(coords[0]), "First");
			Handles.Label(transform.TransformPoint(coords[NbCoords-1]), "Last");
		}
		#endif
	}

	private static float GetMaxY(List<Vector3> coords)
	{
		float result = float.MinValue;
		for(int i = 0; i < coords.Count; ++i)
		{
			if(result < coords[i].y)
				result = coords[i].y;
		}
		return result;
	}

	private static float GetMinY(List<Vector3> coords)
	{
		float result = float.MaxValue;
		for(int i = 0; i < coords.Count; ++i)
		{
			if(result > coords[i].y)
				result = coords[i].y;
		}
		return result;
	}

	public void Reset()
	{
		CreateArea(ref mesh, coords);
	}
	
	public bool ContainsPoint( Vector3 v )
	{
		v = transform.InverseTransformPoint(v);
		
		if(v.y < GetMinY(coords) || v.y > GetMaxY(coords))
			return false;
	
		if(NbCoords < 3) 
			return false;
		
		Vector3 p1 = coords[0];
		Vector3 p2 = coords[1];
		Vector3 p3 = coords[2];
		float determ = (p2.x - p1.x) * (p3.z - p1.z) - (p3.x - p1.x) * (p2.z - p1.z);
		int nbPoints = NbCoords;
		bool bResult = false;
	
		if (determ >= 0.0f) // counter clock wise
		{
			int i = nbPoints-1, j = 0;
			for (; i >= 0; i--) 
			{
				Vector3 pi = coords[i];
				Vector3 pj = coords[j];
				if ((pi.z < v.z && pj.z >= v.z) || (pj.z < v.z && pi.z >= v.z)) 
					if (pi.x+(v.z-pi.z)/(pj.z-pi.z)*(pj.x-pi.x) < v.x) 
						bResult = !bResult; 
				j = i; 
			}
		}
		else // clock wise
		{
			// code copied from http://alienryderflex.com/polygon/
			int i = 0, j = nbPoints-1;
			for (; i < nbPoints; ++i) 
			{
				Vector3 pi = coords[i];
				Vector3 pj = coords[j];
				if ((pi.z < v.z && pj.z >= v.z) || (pj.z < v.z && pi.z >= v.z)) 
					if (pi.x+(v.z-pi.z)/(pj.z-pi.z)*(pj.x-pi.x) < v.x) 
						bResult = !bResult; 
				j = i; 
			}
		}
		return bResult;
	}
	
	public void RemoveCoords(int number)
	{
		if(number > NbCoords - 2) return;
		coords.RemoveRange((NbCoords - number), number);
		CreateArea(ref mesh, coords);
	}
	
	public void AddNewCoords(int number)
	{
		Vector3 lastCoord = Vector3.zero;
		if(NbCoords > 0) // duplicate the last coord
			lastCoord = coords[NbCoords-1];
		for(int i = 0; i < number; ++i) // add the given number of new coords
		{
			if(NbCoords <= 1) // if the mesh does not exist yet
				AddCoord(lastCoord);
			else
				coords.Add(lastCoord);
		}
		if(NbCoords > 2)
			CreateArea(ref mesh, coords);
	}
	
	public void AddCoord(Vector3 v)
	{
		v = transform.InverseTransformPoint(v);
		if(ContainsPoint(v))
			return;
		coords.Add(v);
		CreateArea(ref mesh, coords);
	}

	public void UpdateArea()
	{
		CreateArea(ref mesh, coords);
	}
	
	#region display & creation
	public static void CreateArea(ref Mesh mesh, List<Vector3> coords)
	{
		if(coords.Count <= 1) return; // at least 2 coords available before we can create a mesh.
		mesh.Clear();
		int nbPoints = coords.Count;
		int nbVertices = nbPoints * 2;
		int nbCapTriangles = ((nbPoints - 2) * 2);
		int nbTriangles = (nbPoints * 2) + nbCapTriangles;
		int nbTriangleIndices = nbTriangles * 3;

		Vector3[] vertices = new Vector3[nbVertices];
		Vector2[] uvs = new Vector2[nbVertices];
		int[] triangles = new int[nbTriangleIndices];
		
		float maxy = GetMaxY(coords);
		float miny = GetMinY(coords);
		if(Mathf.Abs(miny-maxy) < 0.01f) maxy = miny + 1.0f;
		
		float uvinc = 1.0f/nbPoints;
		
		for(int i = 0; i < nbPoints; ++i)
		{
			vertices[i] = coords[i];
			vertices[i].y = maxy;
			vertices[i+nbPoints] = coords[i];
			vertices[i+nbPoints].y = miny;
			
			uvs[i] = new Vector2(i*uvinc, 0);
			uvs[i+nbPoints] = new Vector2(i*uvinc, 1);
		}
		
		if(nbPoints > 2)
		{
			if(Determinant(0,1,2,coords.ToArray()) != PolygonOrientation.ClockWise){
				for(int i = 0; i < nbPoints; ++i)
				{
					Vector3 t = vertices[i];
					vertices[i] = vertices[i+nbPoints];
					vertices[i+nbPoints] = t;
				}
			}
			
			TriangulatePolygon(nbPoints,vertices,ref triangles);
			int nbStartIndices = (nbCapTriangles/2)*3;
			for(int ui = nbStartIndices; ui < nbCapTriangles*3; ui+=3){
				triangles[ui] = (triangles[ui - nbStartIndices] + nbPoints) % nbVertices;
				triangles[ui+1] = (triangles[ui+2 - nbStartIndices] + nbPoints) % nbVertices;
				triangles[ui+2] = (triangles[ui+1 - nbStartIndices] + nbPoints) % nbVertices;
			}
		}
		
		int start = nbPoints > 2 ? nbCapTriangles * 3 : 0;
		for(int i = start, t = 0; t < nbPoints; ++t)
		{
			triangles[i++] = t;
			triangles[i++] = t + nbPoints;
			triangles[i++] = (t + 1) % nbPoints;
					
			triangles[i++] = (t + 1) % nbPoints;
			triangles[i++] = t + nbPoints;
			triangles[i++] = nbPoints + ((t + 1) % nbPoints);
		}

		mesh.vertices = vertices;
		mesh.uv = uvs;
		mesh.triangles = triangles;
		mesh.RecalculateNormals();
		mesh.RecalculateBounds();
	}
	
	private enum PolygonOrientation {
		CounterClockWise,
		ClockWise
	};
	
	/// <summary>
	/// Return either clockwise or counter_clockwise for the orientation of the polygon.
	/// </summary>
	private static PolygonOrientation Orientation(int n, Vector3[] v)
	{
		float area = v[n-1].x * v[0].z - v[0].x * v[n-1].z;
		for (int i = 0; i < n-1; ++i)
			area += v[i].x * v[i+1].z - v[i+1].x * v[i].z;
		if (area >= 0.0f)
			return PolygonOrientation.CounterClockWise;
		return PolygonOrientation.ClockWise;
	}
	
	/// <summary>
	/// Computes the determinant of the three points.
	/// Returns whether the triangle is clockwise or counter-clockwise.
	/// </summary>
	private static PolygonOrientation Determinant(int p1, int p2, int p3, Vector3[] v )
	{
		float determ = (v[p2].x - v[p1].x) * (v[p3].z - v[p1].z) - (v[p3].x - v[p1].x) * (v[p2].z - v[p1].z);
		if (determ >= 0.0f)
			return PolygonOrientation.CounterClockWise;
		return PolygonOrientation.ClockWise;
	}
	
	/// <summary>
	/// Returns the square of the distance between the two points
	/// </summary>
	private static float SquaredDistance( float x1,float z1,float x2,float z2 )
	{
		float xd = x1 - x2;
		float zd = z1 - z2;
		return xd * xd + zd * zd;
	}
	
	/// <summary>
	/// Returns true if no other point in the vertex list is inside
	/// the triangle specified by the three points.
	/// </summary>
	private static bool NoInterior(int p1,int p2,int p3, Vector3[] v, int[] vp, int n, PolygonOrientation poly_or )
	{
		for (int i = 0; i < n; ++i) {
			int p = vp[i];              // The point to test
			if ((p == p1) || (p == p2) || (p == p3))
				continue;           // Don't bother checking against yourself
			if (   (Determinant( p2, p1, p, v ) == poly_or)
				|| (Determinant( p1, p3, p, v ) == poly_or)
				|| (Determinant( p3, p2, p, v ) == poly_or) ) {
				} else {
				return false;           // The point is inside
			}
		}
		return true;                   // No points inside this triangle
	}
	
	/// <summary>
	/// Call this procedure with a polygon, this divides it into triangles
	/// Note that this does not work for polygons with holes or self penetrations.
	/// </summary>
	private static void TriangulatePolygon(int n, Vector3[] v, ref int[] indices)
	{
		int[] vp = new int[v.Length];              // Pointers to vertices still left
		int i;                      // Iterative counter

		PolygonOrientation poly_orientation = Orientation( n, v );
	
		for (i = 0; i < n; ++i)
			vp[i] = i;              // Put vertices in order to begin
	
		// Slice off clean triangles until nothing remains
		int tindex = 0;
		int count = n;
		while (count > 3) {
			float min_dist = float.MaxValue;             // Minimum distance so far
			int min_vert = 0;               // Vertex with minimum distance
			int prev;        // Three points currently being considered
			int next;        // Three points currently being considered
			for (int cur = 0; cur < count; ++cur) {
				prev = cur - 1;
				next = cur + 1;
				if (cur == 0)       // Wrap around on the ends
					prev = count - 1;
				else if (cur == count - 1)
					next = 0;
				// Pick out shortest distance that forms a good triangle
				float dist;                 // Distance across this one
				if (   (Determinant( vp[prev], vp[cur], vp[next], v ) == poly_orientation)
					// Same orientation as polygon
					&& NoInterior( vp[prev], vp[cur], vp[next], v, vp, count, poly_orientation )
					// No points inside
					&& ((dist = SquaredDistance( v[vp[prev]].x, v[vp[prev]].y,
					v[vp[next]].x, v[vp[next]].y )) < min_dist) )
					// Better than any so far
				{
					min_dist = dist;
					min_vert = cur;
				}
			} 
	
			// The following error should "never happen".
			if (min_dist == float.MaxValue)
				return;

			prev = min_vert - 1;
			next = min_vert + 1;
			if (min_vert == 0)      // Wrap around on the ends
				prev = count - 1;
			else if (min_vert == count - 1)
				next = 0;
	
			// Output this triangle
		
			indices[tindex++] = vp[prev];
			indices[tindex++] = vp[min_vert];
			indices[tindex++] = vp[next];
			// Remove the triangle from the polygon
			count -= 1;
			for (i = min_vert; i < count; i++)
				vp[i] = vp[i+1];
		}
	
		// Output the final triangle
		indices[tindex++] = vp[0];
		indices[tindex++] = vp[1];
		indices[tindex] = vp[2];
	}
	#endregion
}

