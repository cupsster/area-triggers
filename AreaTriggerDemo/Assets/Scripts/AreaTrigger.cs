using UnityEngine;
using System.Collections.Generic;

public class AreaTrigger : AreaCollider
{
	private class ColliderInfo
	{
		public Collider collider;
		public Transform transform;
		public bool insideArea;

		public void Reset()
		{
			collider = null;
			transform = null;
			insideArea = false;
		}
	}

	private static readonly Stack<ColliderInfo> pool = new Stack<ColliderInfo>(); 
	private readonly Dictionary<Collider, ColliderInfo> insideObjects = new Dictionary<Collider, ColliderInfo>();

	public new void OnEnable()
	{
		base.OnEnable();
		MeshCollider meshCollider = gameObject.GetComponent<MeshCollider>();
		meshCollider.convex = true;
		meshCollider.isTrigger = true;
	}

	public void OnTriggerEnter(Collider other)
	{
		ColliderInfo info = pool.Count > 0 ? pool.Pop() : new ColliderInfo();
		info.collider = other;
		info.transform = other.transform;
		if (ContainsPoint(other.transform.position))
		{
			info.insideArea = true;
			gameObject.SendMessage("OnAreaTriggerEnter", other, SendMessageOptions.DontRequireReceiver);
		}
		else
		{
			info.insideArea = false;
		}
		insideObjects.Add(other, info);

	}

	public void OnTriggerExit(Collider other)
	{
		ColliderInfo info = insideObjects[other];
		insideObjects.Remove(other);
		gameObject.SendMessage("OnAreaTriggerExit", info.collider, SendMessageOptions.DontRequireReceiver);
		info.Reset();
		pool.Push(info);
	}

	public void OnTriggerStay(Collider other)
	{
		ColliderInfo info = insideObjects[other];
		if (ContainsPoint(info.transform.position))
		{
			if (info.insideArea)
				gameObject.SendMessage("OnAreaTriggerStay", info.collider, SendMessageOptions.DontRequireReceiver);
			else
			{
				info.insideArea = true;
				gameObject.SendMessage("OnAreaTriggerEnter", info.collider, SendMessageOptions.DontRequireReceiver);
			}
		}
		else
		{
			if (info.insideArea)
			{
				info.insideArea = false;
				gameObject.SendMessage("OnAreaTriggerExit", info.collider, SendMessageOptions.DontRequireReceiver);
			}
		}
	}
}
