﻿Shader "Hidden/AreaTrigger" 
{
	SubShader 
	{
		Tags { "Queue" = "Transparent" }
		Pass 
		{
			Blend SrcAlpha OneMinusSrcAlpha 
			ZWrite Off 
			Cull Off
			
			CGPROGRAM
			#include "UnityCG.cginc"
			#pragma vertex vert
			#pragma fragment frag
 
			struct appdata {
				float4 vertex : POSITION;
			};

			struct v2f {
				float4 pos : SV_POSITION;
			};
	
			v2f vert(appdata v) 
			{
				v2f o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				return o;
			}

			float4 frag(v2f o) : COLOR
			{
				return fixed4(0,0,1,0.2f);
			}

			ENDCG
		}
	}
}
